import requests
import json
import os

def selectFile():
  """List JSON files available in the current folder"""
  list = os.listdir('.')

  print("\nPlease, select a file from the following list to perform a search:\n")
  a = 1
  files = {}
  for l in list:
    if l.endswith(".json"):
      files[a] = l
      print("\t" + str(a) + ") " + l)
      a += 1 
  question = int(input("\nOption: "))
  selectedFile = files[question]
  return selectedFile

def main():
  """Launcher."""
  f = selectFile()

  # Open JSON file 
  f = open(f) 
    
  # returns JSON object as a dictionary 
  data = json.load(f) 

  # String to filter
  search = input("\nText to search: ")
  print("\n\t=========> Searched Text: " + search + " <=========\n")

  # Iterating through the json list 
  a = 1
  for i in data['pages']:    
    url = i['url']
    if search in url:
      #print(str(a) + ")\t" + url + "\t[" + numCookies + "]")
      #print(str(a) + ")\t" + str(muestraCookies(idUrl)))

      print(str(a) + ")\tURL: " + url)
      a += 1
      
  print("")

  # Closing file 
  f.close()
  
# Main function
if __name__ == "__main__":
  main()
