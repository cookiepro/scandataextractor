import requests
import json
import glob
import math
from json2html import *

def selectFile():
  """List JSON files available in the current folder"""
  list = glob.glob('*.json')
  lenList = [i+1 for i in range(len(list))]

  print("\nPlease, select a file from the following list to perform a search:\n")
  a = 1
  files = {}
  for l in list:
    if l.endswith(".json"):
      files[a] = l
      print("\t" + str(a) + ") " + l)
      a += 1
  question = int(input("\nOption: "))
  selectedFile = files[question]
  return selectedFile

def readJsonFile(fn):
  """Read JSON file and returns content"""
  # Open JSON file 
  with open(fn) as json_file:
    data = json.load(json_file)
    return data
 
def saveHtml2File(data):
  """Save HTML data to a file"""
  # Open JSON file 
  fname = "index.html"
  f = open(fname, 'w') 
    
  # Write data to HTML file
  # <head> data
  head = "<head><!-- Required meta tags --><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><!-- Bootstrap CSS --><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\"><script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css\"><script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js\"></script><script>$(document).ready( function () {$('#table_id').DataTable();} );</script></head>"
  #head = "<head><!-- Required meta tags --><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><!-- Bootstrap CSS --><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\"></head>"
  f.write(head)

  # Table data
  data_buena = data.replace("<table id=\"table_id\" class=\"display table table-sm thead-dark table-bordered table-striped table-hover\">","<table>", 1)
  #data_buena = data.replace("<table id=\"table_id\" class=\"table table-sm thead-dark table-bordered table-striped table-hover\">","<table>", 1)
  f.write(data_buena)
  
  # Close file
  f.close()
  
  print("\nList saved as \"" + fname + "\".")
  return

def main():
  """Launcher""" 
  # Main title
  print("--------------------------------------------------------------------------")
  print("------------------------------ JSON to HTML ------------------------------")
  print("--------------------------------------------------------------------------")
  
  # Read JSON file
  fileName = selectFile()
  content = readJsonFile(fileName)

  # Create HTML code from JSON results data to a <filename>.html file
  htmldata = json2html.convert(json = content, table_attributes="id=\"table_id\" class=\"display table table-sm thead-dark table-bordered table-striped table-hover\"")
  saveHtml2File(htmldata)
  
  return

if __name__ == "__main__":
  main()