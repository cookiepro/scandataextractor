import requests
import json
import math
from json2html import *

def getHeaders(token, cookie):
  """Returns a headers object"""

  token = "Bearer " + token
  cookie = "driftt_aid=28470030-9e9b-4064-8df4-9c86290d34fa; DFTT_END_USER_PREV_BOOTSTRAPPED=true; drift_aid=28470030-9e9b-4064-8df4-9c86290d34fa; _ga=GA1.1.286997758.1611595823; _ga_NL7LPL6ZTS=GS1.1.1611595823.1.1.1611595917.30; OptanonConsent=isIABGlobal=false&datestamp=Tue+Jan+26+2021+00%3A01%3A11+GMT%2B0100+(hora+est%C3%A1ndar+de+Europa+central)&version=6.9.0&hosts=&consentId=2a8e8b4b-501b-4dbd-a73f-bec47ca45b40&interactionCount=1&landingPath=NotLandingPage&groups=1%3A1%2C2%3A0%2C3%3A0%2C4%3A0&AwaitingReconsent=false; __cfduid=d285eaa1590b80999926a8fdc5348586a1611677218; __Secure-fgpt=" + cookie

  headers = {
  'Accept': 'application/json, text/plain, */*',
  'Accept-Language': 'en-us',
  'Authorization': token,
  'Cache-Control': 'no-cache',
  'Cookie': cookie,
  'Expires': '86400',
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-origin'
  }

  return headers

def getToken():
  url = "https://app.cookiepro.com/api/access/v1/oauth/token"
  payload={
    'grant_type': 'password',
    'scope': 'read',
    'username': 'support@pers.eus',
    'password': 'a4Jk*mn8',
    'client_id': 'onetrust'}
  files=[]
  headers = {
    'Cookie': '__cfduid=d11fab5a3b96d07ac29be4d1f7e2009ff1611309484; __Secure-fgpt=8NZmYxVsEvxftLJ9VnJWwCp2T1AnvuPM'
  }
  response = requests.request("POST", url, headers=headers, data=payload, files=files)

  # Obtain cookie value
  tmp = response.headers
  tmp = tmp['Set-Cookie']
  tmp = tmp.split("; ")
  cookie = tmp[0].replace('\'', '')
  cookie = cookie.replace('__Secure-fgpt=', '', 1)

  # Obtain token and refresh_token values
  jsonData = json.loads(response.text)
  token = jsonData['access_token']
  refresh = jsonData['refresh_token']

  return(token,cookie,refresh)

def refreshToken(rf, cookie):
  url = "https://app.cookiepro.com/api/access/v1/oauth/token"
  payload={
    'grant_type': 'refresh_token',
    'client_id': 'onetrust',
    'scope': 'read',
    'refresh_token': rf
    }
  files=[]

  cookie = "driftt_aid=28470030-9e9b-4064-8df4-9c86290d34fa; DFTT_END_USER_PREV_BOOTSTRAPPED=true; drift_aid=28470030-9e9b-4064-8df4-9c86290d34fa; _ga=GA1.1.286997758.1611595823; _ga_NL7LPL6ZTS=GS1.1.1611595823.1.1.1611595917.30; OptanonConsent=isIABGlobal=false&datestamp=Tue+Jan+26+2021+00%3A01%3A11+GMT%2B0100+(hora+est%C3%A1ndar+de+Europa+central)&version=6.9.0&hosts=&consentId=2a8e8b4b-501b-4dbd-a73f-bec47ca45b40&interactionCount=1&landingPath=NotLandingPage&groups=1%3A1%2C2%3A0%2C3%3A0%2C4%3A0&AwaitingReconsent=false; __cfduid=d285eaa1590b80999926a8fdc5348586a1611677218; __Secure-fgpt=" + cookie
  headers = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'en-us',
    'Cache-Control': 'no-cache',
    'Cookie': cookie,
    'Expires': '86400',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin'
  }
  response = requests.request("POST", url, headers=headers, data=payload, files=files)
  
  # Obtain cookie value
  tmp = response.headers
  tmp = tmp['Set-Cookie']
  tmp = tmp.split("; ")
  n_cookie = tmp[0].replace('\'', '')
  n_cookie = n_cookie.replace('__Secure-fgpt=', '', 1) 
  
  # Obtain new token and new refresh_token values
  jsonData = json.loads(response.text)
  n_token = jsonData['access_token']
  n_refresh = jsonData['refresh_token']

  return(n_token,n_cookie,n_refresh)

def getDomainData(token, cookie):
  """Get Domain - DomainID -ScanID data"""
  # URL websites list
  #url = "https://app.cookiepro.com/api/cookiemanager/v1/websitescans?searchStr=&page=0&size=20&sort=domain.Url,ASC"
  url = "https://app.cookiepro.com/api/cookiemanager/v1/websitescans?searchStr=&page=0&size=20&sort=Url,ASC"
  headers = getHeaders(token, cookie)
  payload={}
  response = requests.request("GET", url, headers=headers, data=payload)
  if response.status_code == 200:
    jsonData = json.loads(response.text)
    #print(json.dumps(jsonData, sort_keys=False, indent=4))
    return jsonData['content']
  else:
    print ('Invalid Token!\n')
    quit()

def showDomainData(domainData):
  """Represent domain data info in a table"""
  print("\n  ===============================================================================================================================================================")
  print("\t\t Domain ID \t\t | \t\t Scan ID \t\t | Scanned Pages | Cookies | \t\t Domain \t\t\t\t")
  print("  ---------------------------------------------------------------------------------------------------------------------------------------------------------------")
  for d in domainData:
    print("    " + d['domainId'] + " | " + d['scanId'] + "  | \t" + str(d['scannedPages']) + "\t |   " + str(d['uniqueCookiesFound']) + "    | " + d['domain'])
  print("  ===============================================================================================================================================================")
  return

def selectDomain(domainData):
  """Select domain"""
  # Iterating through the domainData list 
  print("\n\tPlease, select one of the following domains:\n") 
  a = 1
  for d in domainData:    
    print("\t\t" + str(a) + ") Domain: " + d['domain'])
    a += 1
  question = int(input("\n\tOption: "))
  res = domainData[question - 1]
  return res

def getDomainUrlList(selectedDomain, size, token, cookie):
  """Download URL list"""
  headers = getHeaders(token, cookie)
  payload={}

  # Calculate how many time pages of 2000 elements we have to retrieve
  numSearch = math.ceil(size/2000)

  # Variable to collect the scanned URL list
  pages = []

  #for x in range(numSearch):
  for x in range(numSearch):
    #limInf = (x*2000) + 1
    #limSup = (x*2000) + 2000

    # URL cookies
    url = "https://app.cookiepro.com/api/cookiemanager/v1/reports/domains/" + selectedDomain['domainId'] + "/scans/" + selectedDomain['scanId'] + "/pageshistory/page?page=" + str(x) + "&size=" + str(size) + "&searchStr=&sortBy=pageUrl&direction=desc"
    response = requests.request("GET", url, headers=headers, data=payload)
    jsonData = json.loads(response.text)
    #jsonData = json.dumps(jsonData,sort_keys=False, indent=4)
    pages.extend(jsonData['pages']['content'])  
  return pages

def getCookies(domainId, scanId, idURL, token, cookie):
  """Displays cookies of a URL."""
  # URL para consultar cookies
  #url = "https://app.cookiepro.com/api/cookiemanager/v1/reports/domains/" + domainId + "/pages/" + idURL + "/cookiesbypage"
  url = "https://app.cookiepro.com/api/cookiemanager/v1/reports/domains/" + domainId + "/scans/" + scanId + "/pages/" + idURL + "/cookiesbypage"
  headers = getHeaders(token, cookie)
  payload={}
  response = requests.request("GET", url, headers=headers, data=payload)
  if response.status_code == 200:
    jsonData = json.loads(response.text)
    return jsonData
  else:
    print ('Invalid Token!\n')
    quit()

def saveJson2File(domainId, data):
  """Save JSON data to a file"""

  # Open JSON file 
  fname = "Listado_total_URL_" + domainId + ".json"
  f = open(fname, 'w') 
    
  # Write data to JSON file
  json.dump(data, f)

  # Close file
  f.close()

  print("\nList saved as \"" + fname + "\".")
  return

def saveHtml2File(domainId, data):
  """Save HTML data to a file"""

  # Open JSON file 
  fname = domainId + ".html"
  f = open(fname, 'w') 
    
  # Write data to HTML file
  # <head> data
  head = "<head><!-- Required meta tags --><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><!-- Bootstrap CSS --><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\"><script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css\"><script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js\"></script><script>$(document).ready( function () {$('#table_id').DataTable();} );</script></head>"
  f.write(head)

  # Table data
  data_buena = data.replace("<table id=\"table_id\" class=\"display table table-sm thead-dark table-bordered table-striped table-hover\">","<table>", 1)
  f.write(data_buena)
  
  # Close file
  f.close()
  
  print("\nList saved as \"" + fname + "\".")
  return

def main():
  """Launcher""" 
  # Get token, cookie and refresh_token values from AUTH Endpoint
  t,c,r = getToken()
  #print("token: " + t)
  #print("Cookie: " + c)
  #print("Refresh-Token: " + r)

  # Collect Domain, DomainID and ScanID data
  domData = getDomainData(t,c)
  showDomainData(domData)

  # Select a domain to download JSON file with URL-Cookies relationship
  sel_dom = selectDomain(domData)
  
  # Obtain URL list of a domain
  numPages = sel_dom['scannedPages']
  print(numPages)
  urlList = getDomainUrlList(sel_dom,numPages,t,c)
  
  # Other values from the user-selected domain
  domId = sel_dom['domainId']
  scanId = sel_dom['scanId']
  domainUrl = sel_dom['domain']
  
  # Iterating through the JSON list 
  print("\nThe following JSON shows the list of Cookies for each of the scanned URLs of \"" + domainUrl + "\":\n")
  counter = 1
  a = 1
  for i in urlList: 
    # Print number of page being analyzed
    print("Page: " + str(counter))

    if a > 250:
      # Refresh: token, cookie and refresh_token
      t,c,r = refreshToken(r,c)
      a = 1
    else:
      idUrl = i['id']
      url = i['auditUrl']

      # URL Data object
      ud = {}
      ud['id'] = idUrl
      ud['url'] = url
      ud['cookies'] = []

      # Checks the cookies in a URL
      cd = getCookies(domId, scanId, idUrl, t, c)

      # Iterating through the JSON list of cookies belonging to a URL
      for l in cd:
        cookie = {}
        
        l_name = l['name']
        l_host = l['host']
        
        cookie['name'] = l_name
        cookie['host'] = l_host
        ud['cookies'].append(cookie)

      results['pages'].append(ud)
      a += 1
      counter += 1

  # Save JSON with the results data to a <filename>.json file
  saveJson2File(domId, results)

  # Create HTML code from JSON results data to a <filename>.html file
  htmldata = json2html.convert(json = results, table_attributes="id=\"table_id\" class=\"display table table-sm thead-dark table-bordered table-striped table-hover\"")
  saveHtml2File(domId, htmldata)
  
  return

if __name__ == "__main__":
  # Global "results" variable
  results = {}
  results['pages'] = []

  main()
  #print(json.dumps(results, sort_keys=False, indent=4))
