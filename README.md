# ScanDataExtractor

Program to extract data from scheduled/performed domain scans.

## Table of Contents
 - [Description](#description)
 - [Installation](#installation)
 - [Usage](#usage)
 - [Contributing](#contributing)
 - [References](#references)

## Description
The aim of the "1-Get_Cookies_from_Scanned_URL_list.py" program is to obtain the list of URL-Cookies; all the cookies detected on each of the URLs of the targeted domain being scanned by CookiePro.
For that, the programs starts by showing a list of the inserted/schedulled domains and the obtained results:
* Domain
* DomainID
* ScanID
* Scanned Pages
* Total of unique Cookies detected

Then, in order to obtain a detailed view of which Cookies are being used on each of the URL, specific requests are launched against the API asking for "Cookies of 'X' URL".


## Installation

Your very first step is to clone this repository:

```zsh
$ git clone https://gitlab.com/cookiepro/scandataextractor.git
```

Then you must install the Python package requirements as follows:
```zsh
$ pip3 install -r requirements.txt
```

## Usage

Prior to execute "1-Get_Cookies_from_Scanned_URL_list.py" program, you must update "username" and "password" values on lines 34 and 35:

```zsh
'username': '<USERNAME_HERE>',
'password': '<PASSWORD_HERE>',
```

Once the credentials are properly configured, launch the program as follows:

```zsh
$ python3 1-Get_Cookies_from_Scanned_URL_list.py
```


## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## References

